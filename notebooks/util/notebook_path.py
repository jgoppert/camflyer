import os
import inspect
import sys

camflyer_root = os.path.join(os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe()))), os.pardir, os.pardir)
sys.path.insert(0, camflyer_root)
