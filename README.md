# Bormatec CamFlyer-Q Control Analysis

## Building Fortran Library

To improve simulation speed, the nonlinear model was 
written in fortran. This can be compiled as shown below

```bash
cd nonlinear_model
./build.sh
```

IPython notebooks are used for the analysis.
Notebook viewer can be used to view the notebooks
without IPython installed:

## IPython Notebook

IPython notebooks are used for the analysis.
Notebook viewer can be used to view the notebooks
without IPython installed:

* http://nbviewer.ipython.org/github/jgoppert/camflyer/tree/master/notebooks/

### Dependencies

Install required systems libraries on debian based distros:

```bash
sudo apt-get install python-pip
sudo apt-get build-dep python-scipy
```

Install required python packages using pip:

```bash
pip install numpy scipy sympy slycot control ipython

```

Note if you get a link error such as

```bash
AttributeError: 'module' object has no attribute 'sb02mt_n'
```

When trying to use slycot functions, you probably didn't install slycot properly.

### Running IPython Notebooks

From the root directory of camflyer.

```bash
ipython notebook notebooks
```

## Computational Fluid Dynamics Model

* XFLR5-6.10 : This is the latest svn version and can be automatically installed by typing

If you have another version installed, uninstall it first.
```bash
sudo apt-get remove xflr5
```

Now download the code using subversion.
```bash
sudo apt-get install subversion git libqt4-dev
svn checkout svn://svn.code.sf.net/p/xflr5/code/trunk xflr5
cd xflr5 && svn update && qmake-qt4 && make -j8
make install
```

Note, you can use checkinstall to create an uninstallable debian package
```bash
sudo apt-get install checkinstall
cd xfrl5
sudo checkinstall
```

The xflr5 file is located in data/camflyer.xfl.
