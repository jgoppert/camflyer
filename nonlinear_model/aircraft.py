import numpy as np
import scipy.integrate
import scipy.optimize
import copy

from camflyer_fortran import camflyer as fortran
from .data import Data


class Aircraft(object):

    default_params = {
        # moment of inertia
        'j_x': 0.01744,
        'j_xz': 5.97e-05,
        'j_y': 0.02538,
        'j_z': 0.04281,
        # mass
        'm': 0.3,
        # environment
        'g_d': 9.8,
        'rho': 1.225,
        # aero references
        's': 0.17188,
        'b': 0.846,
        'c_bar': 0.20853,
        # side coefficients
        'cc_beta': -0.1,
        # drag polar
        'cd_dm': 0.019667461390396053,
        'k_cd_cl': 1.2107577207997946,
        'alpha_dm': 0.025249385799798855,
        # lift curve
        'cl_0': -0.04157168542825352,
        'cl_alpha': 3.8312392965007041,
        # aero moment coefficients
        'cl_beta': -0.09059,
        'cl_da': 1,
        'cl_p': -0.47182,
        'cm_0': 0.1,
        'cm_alpha': -0.1,
        'cm_de': 1,
        'cm_q': -2.25786,
        'cn_beta': 0.04121,
        'cn_r': -0.01522,
        # TODO
        # 'cog_x': 0.1285,
        # 'cog_z': 7.443e-05,
        # left/right cntrl
        'delta_l_tau': 0.1,
        'delta_r_tau': 0.1,
        # elevon max deflect
        'elvn_max': 0.25,
        # propulsion
        'thrust_max': 1,
        'delta_t_tau': 0.1,
        # gps
        'gps_period': 1.0/2,
        'gps_v_std': 0.0,
        'gps_v_res': 0.001,
        'gps_v_max': 100,
        'gps_p_std': 0.0,
        'gps_p_res': 0.001,
        'gps_p_max': 0.01,
        'gps_h_std': 0.0,
        'gps_h_res': 0.01,
        'gps_h_min': -1e6,
        'gps_h_max': 1e6,
        # baro
        'baro_period': 1.0/10,
        'baro_std': 0.0,
        'baro_res': 0.01,
        'baro_min': -1e6,
        'baro_max': 1e6,
        # pitot
        'pitot_period': 1.0/10,
        'pitot_std': 0.0,
        'pitot_res': 0.01,
        'pitot_min': 0,
        'pitot_max': 100,
        # mag
        'mag_period': 1.0/50,
        'mag_std': 0.0,
        'mag_res': 0.01,
        'mag_max': 1e3,
        # gyro
        'gyro_period': 1.0/200,
        'gyro_std': 0.0,
        'gyro_res': 0.01,
        'gyro_max': 1e3,
        # accel
        'accel_period': 1.0/200,
        'accel_std': 0.0,
        'accel_res': 0.01,
        'accel_max': 1e3,
    }

    def __init__(self, tf=100.0, params=default_params):
        """
        tf : final time (sec)

        This  initialize the aircraft sim and allocates
        all of the data structures.
        """

        # TODO move to params?, not directly used in fortran
        # sim so leaving out for now, used on python side
        process_noise_power = 0.0

        # simulation time
        # for noise to have sufficient bandwidth should run sim at
        # 100*f_max, where f_max is the fastest closed loop pole
        # frequency, for our system the actuators are 10 Hz and will
        # likely be the fastest poles so we will simulate at
        # 1000 Hz
        dt = 1.0/1000
        n_t = int((tf-dt)/dt)

        # dynamics function
        f_dynamics = lambda t, x, u, y, measure, y_noise, params: \
            fortran.dynamics(
                t=t, x_vect=x, u_vect=u, y=y,
                measure=measure,
                y_noise=y_noise,
                **params)

        # setup integration
        sim = scipy.integrate.ode(f_dynamics)
        sim.set_integrator('dopri5')

        # setup data structure
        data = Data(n_t)

        # save data to class
        self.sim = sim
        self.data = data
        self.process_noise_power = process_noise_power
        self.dt = dt
        self.data = data
        self.n_t = n_t
        self.params = params
        self.f_dynamics = f_dynamics

    def params_for_fortran(self):
        return {key.lower(): self.params[key] for key in self.params.keys()}

    def trim_steady_level(self, V_T, xd_guess=[0, 0, 0, 0.5]):

        # initial guess
        u0 = np.zeros(Data.n_u)
        u0 = u0.view(dtype=Data.u_dtype, type=np.recarray)

        x0 = np.zeros(Data.n_x)
        x0 = x0.view(dtype=Data.x_dtype, type=np.recarray)

        def cost(xd, get_trim=False):

            alpha = xd[0]
            delta_l = xd[1]
            delta_r = xd[2]
            delta_t = xd[3]

            theta = alpha  # flight path angle zero

            u0.delta_l = delta_l
            u0.delta_r = delta_r
            u0.delta_t = delta_t

            q_bn = fortran.euler_to_quaternion(0.0, theta, 0.0)

            x0.U = V_T*np.cos(alpha)
            x0.W = -V_T*np.sin(alpha)
            x0.a = q_bn[0]
            x0.b = q_bn[1]
            x0.c = q_bn[2]
            x0.d = q_bn[3]
            x0.x_l = delta_l
            x0.x_r = delta_r
            x0.x_t = delta_t

            dx, y_update = self.f_dynamics(
                t=0,
                x=x0.view(dtype='f8'),
                u=u0.view(dtype='f8'),
                y=np.zeros(Data.n_y),
                measure=False,
                y_noise=np.zeros(Data.n_y_noise),
                params=self.params_for_fortran())

            dx = dx.view(dtype=Data.x_dtype, type=np.recarray)

            if get_trim:
                return x0, u0, dx
            else:
                s = dx.P*dx.P + dx.Q*dx.Q + dx.R*dx.R \
                    + dx.U*dx.U + dx.V*dx.V + dx.W*dx.W \
                    + dx.h*dx.h
                return s

        elvn_max = self.params['elvn_max']
        bnds = (  # alpha delta_l delta_r delta_t
               (np.deg2rad(-10), np.deg2rad(10)),
               (-elvn_max, elvn_max),
               (-elvn_max, elvn_max),
               (0, 1))
        res = scipy.optimize.minimize(
            cost, xd_guess, method='Nelder-Mead',
            bounds=bnds)
        print res
        xs = res.x
        x0, u0, dx = cost(xs, get_trim=True)
        return x0, u0, dx

    def simulate(self, x0=None, u0=None):

        if (x0 is None or u0 is None):
            x0, u0, dx = self.trim_steady_level(10.0)
            print x0, u0, dx

        # get class data
        sim = self.sim
        process_noise_power = self.process_noise_power
        dt = self.dt
        data = self.data
        n_t = self.n_t

        # intialize loop
        sim.set_initial_value(x0.view(dtype='f8'))
        fortran.normalize_quaternions(sim.y)
        u = copy.deepcopy(u0.view(dtype='f8'))
        i = 0

        y = np.zeros(Data.n_y, dtype='f8')
        y_update = np.zeros(Data.n_y_update, dtype='bool')

        # initialize process noise vector
        w = np.zeros(len(sim.y), order='F')

        # TODO add wind
        # wind_n = 0
        # wind_e = 0
        # wind_d = 0

        # get current params, lower keys
        params = self.params_for_fortran()

        # main simulation loop
        while True:

            # send parameters to fortran simulation function
            # fort_params.update({'u_vect': u})
            sim.set_f_params(u, y, False, np.zeros(Data.n_y_noise), params)

            # integrate continuous dynamics
            sim.integrate(sim.t + dt)
            if not sim.successful():
                print 'sim failed'
                print sim.t, sim.y
                break

            # add process noise
            # TODO is quaternion noise white?
            w = process_noise_power*dt*np.random.randn(Data.n_x)
            sim.set_initial_value(sim.y + w, sim.t)

            # normalize quaterions
            fortran.normalize_quaternions(sim.y)

            # take measurements
            dx, y_update = self.f_dynamics(
                t=sim.t, x=sim.y, u=u, y=y,
                measure=True,
                y_noise=np.random.rand(Data.n_y_noise), params=params)

            # yp_update_i = data.y_update.view(
            #     dtype=Data.y_update_dtype, type=np.recarray)

            # save data
            data.set(i, sim.t, sim.y, u, y, y_update)

            # exit if done
            if (i+1 >= n_t):
                break
            else:
                i += 1

            # calculate new control
            # yp = y.view(dtype=Data.y_dtype, type=np.recarray)
            # delta_a = -0.1*yp.phi
            # delta_e = -0.1*yp.theta
            # V_T = np.sqrt(x0.U*x0.U + x0.V*x0.V + x0.W*x0.W)
            # delta_t = -0.1*(V_T - yp.V_T) + u0.delta_t
            # # mixing
            # delta_l = delta_e + delta_a + u0.delta_l
            # delta_r = delta_e - delta_a + u0.delta_r
            #
            # # set new input
            # u = [delta_l, delta_r, delta_t, wind_n, wind_e, wind_d]

        data.truncate(i)


def profile():
    import cProfile
    import pstats
    import StringIO
    pr = cProfile.Profile()
    ac = Aircraft()
    pr.enable()
    ac.simulate()
    pr.disable()
    s = StringIO.StringIO()
    sortby = 'cumulative'
    ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    ps.print_stats()
    print s.getvalue()
