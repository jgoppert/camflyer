from .zoh import ZeroOrderHold


class Autopilot(object):
    """
    This roughly approximates the software running in
    discrete time on the autopilot.
    """

    def __init__(self, x0):
        """
        Initialize the autopilot states etc.
        """

        self.dt_observer = 1.0/200
        self.dt_controller = 1.0/50

        self.zoh_controller = ZeroOrderHold(period=self.dt_controller)
        self.zoh_observer = ZeroOrderHold(period=self.dt_observer)

        self.x = x0

    def observer(self, t, u, y, zoh_gps):
        """
        This function is the observer.
        """
        x0 = self.x
        x = x0 + (x0 + u)*self.dt_observer
        if zoh_gps.updated:
            zoh_gps.updated = False
            x = x + 1*(y - x)
        self.x = x
        return x

    def controller(self, t, x):
        """
        This function is the controller.
        """
        u = -2*x
        return u

    def update(self, t, y, zoh_gps):
        """
        This function simulates the operating system scheduler
        running on the autopilot.
        """
        # controller
        u = self.zoh_controller.update(t, lambda: self.controller(t, self.x))
        # observer
        self.x = self.zoh_observer.update(
            t, lambda: self.observer(t, u, y, zoh_gps))
        return u
