import numpy as np


class Data(object):
    """
    This is the simulation data structure.
    """
    x_names = 'U,V,W,a,b,c,d,P,Q,R,p_N,p_E,h,'\
        'x_l,x_r,x_t,gyro_b_x,gyro_b_y,gyro_b_z'.split(',')
    n_x = len(x_names)
    x_titles = r'$U$,$V$,$W$,$a$,$b$,$c$,$d$,'\
               r'$P$,$Q$,$R$,$p_N$,$p_E$,$h$,'\
               r'$x_l$,$x_r$,$x_t$,gyro $b_x$,'\
               r'gyro $b_y$, gyro $b_z$'.split(',')
    x_dtype = np.dtype({
        'names': x_names, 'titles': x_titles,
        'formats': ['f8']*n_x})

    u_names = 'delta_l,delta_r,delta_t,wind_x,wind_y,wind_z'.split(',')
    u_titles = r'$delta_l$,$delta_r$,$delta_t$,'\
               r'$wind_x$,$wind_y$,$wind_z$'.split(',')
    n_u = len(u_names)
    u_dtype = np.dtype({
        'names': u_names, 'titles': u_titles,
        'formats': ['f8']*n_u})

    y_names = r'V_T,alpha,beta,phi,theta,psi,'\
        r't_gps,V_N,V_E,V_D,p_N,p_E,h,'\
        r't_mag,B_x,B_y,B_z,'\
        r't_gyro,P,Q,R,'\
        r't_accel,a_x,a_y,a_z,'\
        r't_baro,P_a,'\
        r't_pitot,P_d'.split(',')
    n_y = len(y_names)
    n_y_noise = 17 # how many measurements that require noise
    y_titles = r'$V_T$,$\alpha$,$\beta$,$\phi$,$\theta$,$\psi$,'\
               r'$t_{GPS}$,$V_N$,$V_E$,$V_D$,$p_N$,$p_E$,$h$,'\
               r'$t_{mag}$,$B_x$,$B_y$,$B_z$,'\
               r'$t_{gyro}$,$P$,$Q$,$R$,'\
               r'$t_{accel}$,$a_x$,$a_y$,$a_z$,'\
               r'$t_{baro}$,$P_a$,'\
               r'$t_{pitot}$,$P_d$'.split(',')
    y_dtype = np.dtype({
        'names': y_names, 'titles': y_titles,
        'formats': ['f8']*n_y})

    y_update_names = 'gps,mag,gyro,accel,baro,pitot'.split(',')
    n_y_update = len(y_update_names)
    y_update_dtype = np.dtype({
        'names': y_update_names,
        'formats': ['bool']*n_y_update})

    def __init__(self, n_t):

        self.n_t = n_t
        self.t = np.zeros(self.n_t, dtype='f8')
        self.x = np.zeros((self.n_t, self.n_x), dtype='f8')
        self.u = np.zeros((self.n_t, self.n_u), dtype='f8')
        self.y = np.zeros((self.n_t, self.n_y), dtype='f8')
        self.y_update = np.zeros(
            (self.n_t, self.n_y_update), dtype='bool')

    def truncate(self, i):

        self.t = self.t[:i+1]
        self.x = self.x[:i+1, :]
        self.u = self.u[:i+1, :]
        self.y = self.y[:i+1, :]
        self.y_update = self.y_update[:i+1, :]

    def set(self, i, t, x, u, y, y_update):
        self.t[i] = t
        self.x[i, :] = x
        self.u[i, :] = u
        self.y[i, :] = y
        self.y_update[i, :] = y_update
