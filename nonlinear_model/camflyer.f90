      module camflyer

      implicit none

      contains

      pure subroutine dynamics( &
                t, &                            ! time
                x_vect, &                       ! state
                u_vect, &                       ! input
                measure, &                      ! do a measurement?
                y_noise, &                      ! noise for measurements if taken
                g_d, m, j_x, j_y, j_z, j_xz, &  ! mass
                rho, s, b, c_bar, &             ! aero
                cl_0, cl_alpha, &               ! lift
                cd_dm, k_cd_cl, alpha_dm, &     ! drag
                cc_beta, &                      ! side force
                cl_beta, cl_p, cl_da, &         ! rolling 
                cm_0, cm_alpha, cm_q, cm_de, &  ! pitching
                cn_beta, cn_r, &                ! yawing
                delta_l_tau, delta_r_tau, &     ! left/right cntrl
                elvn_max, &                     ! elevon max deflect.
                thrust_max, delta_t_tau, &      ! propulsion
                gps_period, gps_v_std, gps_v_res, gps_v_max, &   ! gps
                gps_p_std, gps_p_res, gps_p_max, gps_h_std, &
                gps_h_res, gps_h_min, gps_h_max, &
                baro_period, baro_std, baro_res, &               ! baro
                baro_min, baro_max, &
                pitot_period, pitot_std, pitot_res, &            ! pitot
                pitot_min, pitot_max, &
                mag_period, mag_std, mag_res, mag_max, &         ! mag
                gyro_period, gyro_std, gyro_res, gyro_max, &     ! gyro
                accel_period, accel_std, accel_res, accel_max, & ! accel
                dx, &                 ! state deriv
                y, y_update)          ! measurements/ meas updates

          ! x: u, v, w, a, b, c, d, p, q, r, p_n, p_e, h
          !    delta_l, delta_r, delta_t,
          !    gyro_bias_x, gyro_bias_y, gyro_bias_z
          ! u: delta_l_cmd, delta_r_cmd, delta_t,
          !    wind_n, wind_e, wind_d
          ! y: v_t, alpha, beta, phi, theta, psi, (aux states)
          !    t_gps, v_n, v_e, v_d, p_n, p_e, h, (gps)
          !    t_mag, b_x, b_y, b_z, (mag)
          !    t_gyro, p, q, r, (gyro)
          !    t_accel, a_x, a_y, a_z, (accel)
          !    t_baro, p_a, (baro)
          !    t_pitot, p_d (pitot)
          ! y_update:
          !    gps, mag, gyro, accel, baro, pitot

          ! input
          real(8), intent(in) :: t
          real(8), dimension(19), intent(in) :: x_vect
          real(8), dimension(6), intent(in) :: u_vect
          logical, intent(in) :: measure
          real(8), dimension(17), intent(in) :: y_noise ! noise for measurements if taken
          real(8), intent(in) :: &
                g_d, m, j_x, j_y, j_z, j_xz, &  ! mass
                rho, s, b, c_bar, &             ! aero
                cl_0, cl_alpha, &               ! lift
                cd_dm, k_cd_cl, alpha_dm, &     ! drag
                cc_beta, &                      ! side force
                cl_beta, cl_p, cl_da, &         ! rolling 
                cm_0, cm_alpha, cm_q, cm_de, &  ! pitching
                cn_beta, cn_r, &                ! yawing
                delta_l_tau, delta_r_tau, &     ! left/right cntrl
                elvn_max, &                     ! elevon max deflect.
                thrust_max, delta_t_tau, &      ! propulsion
                gps_period, gps_v_std, gps_v_res, gps_v_max, &   ! gps
                gps_p_std, gps_p_res, gps_p_max, gps_h_std, &
                gps_h_res, gps_h_min, gps_h_max, &
                baro_period, baro_std, baro_res, &               ! baro
                baro_min, baro_max, &
                pitot_period, pitot_std, pitot_res, &            ! pitot
                pitot_min, pitot_max, &
                mag_period, mag_std, mag_res, mag_max, &         ! mag
                gyro_period, gyro_std, gyro_res, gyro_max, &     ! gyro
                accel_period, accel_std, accel_res, accel_max    ! accel

          ! output
          real(8), dimension(19), intent(out) :: dx
          real(8), intent(inout), dimension(29) :: y
          logical, dimension(6), intent(out) :: y_update

          ! local variables
          real(8), dimension(3,3) :: c_bw, c_bn
          real(8) :: p_n, p_e, h, &
                u, v, w, &
                delta_l, delta_r, delta_t, &
                delta_l_cmd, delta_r_cmd, delta_t_cmd, &
                gyro_bias_x, gyro_bias_y, gyro_bias_z, &
                gam, q_bar, p, q, r, de, da
          real(8), dimension(3,3) :: c_nb ! dcm from body to nav
          real(8), dimension(4) :: q_nb ! quat. b frame
          real(8), dimension(3) :: f_b, m_b ! force/mom. b frame
          real(8), dimension(3) :: fa_w, ma_w ! aero force/mom. wind frame
          real(8), dimension(3) :: ft_b, mt_b ! prop force/mom. body frame
          real(8), dimension(4) :: dq_nb ! quaternion derivative
          real(8), dimension(3) :: omega_nb_b ! ang. vel of b wrt n in b
          real(8), dimension(3) :: wind_n ! wind in nav frame
          real(8), dimension(3) :: wind_b ! wind in body frame
          real(8), dimension(3) :: v_rel ! vel rel. to wind
          real(8), dimension(3) :: v_b ! inertial vel in body frame
          real(8), dimension(3) :: dv_b ! deriv of vel in body frame
          real(8), dimension(3) :: accel_b ! accel measurement, in body
          real(8), dimension(3) :: b_n ! magnetic field in nav frame
          real(8), dimension(3) :: b_b ! magnetic field in body frame
          real(8), dimension(3) :: v_n ! inertial vel in nav frame
          real(8), dimension(3,3) :: j_b ! mom. of inertia in body frame
          real(8), dimension(3,3) :: inv_j_b ! inverse
          real(8), dimension(3) :: g_n ! gravity in inertial frame
          real(8) :: cl, cd, cc, c_l, c_m, c_n ! aero coefficients

          ! updates gps, mag, gyro, accel, baro, pitot
          integer, parameter :: i_gps=1, i_mag=2, i_gyro=3, &
                i_accel=4, i_baro=5, i_pitot=6

          ! measurement data
          real(8) :: v_t, alpha, beta, phi, theta, psi, &
                  t_gps, y_v_n, y_v_e, y_v_d, y_p_n, y_p_e, y_h, &
                  t_mag, y_b_x, y_b_y, y_b_z, &
                  t_gyro, y_p, y_q, y_r, &
                  t_accel, y_a_x, y_a_y, y_a_z, &
                  t_baro, y_p_a, &
                  t_pitot, y_p_d

          ! extract state vector

          ! velocity in body frame
          v_b = x_vect(1:3)
          u = v_b(1)
          v = v_b(2)
          w = v_b(3)

          ! attitude quaternion
          q_nb = x_vect(4:7)

          ! angular velocity in body frame
          omega_nb_b = x_vect(8:10)
          p = omega_nb_b(1)
          q = omega_nb_b(2)
          r = omega_nb_b(3)

          ! position
          p_n = x_vect(11)
          p_e = x_vect(12)
          h = x_vect(13)

          ! actuator positions
          delta_l = x_vect(14)
          delta_r = x_vect(15)
          delta_t = x_vect(16)

          ! gyro biases
          gyro_bias_x = x_vect(17)
          gyro_bias_y = x_vect(18)
          gyro_bias_z = x_vect(19)

          ! extract input vector
          delta_l_cmd = u_vect(1)
          delta_r_cmd = u_vect(2)
          delta_t_cmd = u_vect(3)
          wind_n = u_vect(4:6)

          ! inertia matrices
          j_b = reshape((/j_x,0d0,-j_xz,&
                          0d0,j_y,0d0,&
                          -j_xz,0d0,j_z/), shape(j_b))
          gam = j_x*j_z - j_xz*j_xz
          inv_j_b = reshape((/j_z,0d0,j_xz,&
                              0d0,gam/j_y,0d0,&
                              j_xz,0d0,j_x/), shape(inv_j_b))/gam

          ! dcm
          call quaternion_to_dcm(q_nb, c_nb)
          c_bn = transpose(c_nb)

          ! euler
          call dcm_to_euler(c_nb, phi, theta, psi)

          ! aerodynamics
          wind_b = matmul(c_bn, wind_n)
          ! prevent singularity in alpha/beta
          if (abs(v_rel(1)) < 0.001d0) then
                v_rel(1) = sign(0.001d0, v_rel(1))
          end if
          v_rel = v_b - wind_b
          v_t = norm2(v_rel)
          q_bar = 0.5d0*rho*v_t*v_t
          alpha = atan2(v_rel(3), v_b(1))
          beta = asin(v_rel(2)/v_t)

          ! wind to body dcm
          call compute_c_bw(alpha, beta, c_bw)

          ! gravity
          g_n(1:2) = 0.0d0
          g_n(3) = g_d

          ! aerodynamics
          cd = cd_dm + k_cd_cl*(alpha - alpha_dm)**2
          cc = cc_beta*beta
          cl = cl_0 + cl_alpha*alpha
          fa_w(1) = -cd*q_bar*s ! drag
          fa_w(2) = -cc*q_bar*s ! side force
          fa_w(3) = -cl*q_bar*s ! lift
          ! mixing for polynomials
          de = (delta_l + delta_r)/2
          da = (delta_l - delta_r)/2
          ! todo is pqr in correct frame for poly fit?
          c_l = cl_p*p + cl_beta*beta &
                + cl_da*da
          c_m = cm_0 + cm_alpha*alpha + cm_q*q &
                + cm_de*de
          c_n = cn_beta*beta + cn_r*r

          ma_w(1) = q_bar*s*b*c_l
          ma_w(2) = q_bar*s*c_bar*c_m
          ma_w(3) = q_bar*s*b*c_n

          ! propulsion
          ft_b(1) = thrust_max*delta_t
          ! write(*,*) 'ft_b(1)', ft_b(1)
          ! write(*,*) 'g_n', g_n
          ft_b(2:3) = 0.0d0
          mt_b(1:3) = 0.0d0

          ! force equations
          f_b = matmul(c_bw, fa_w)  + ft_b
          dv_b = f_b/m  - matmul(c_bn, g_n) - cross(omega_nb_b, v_b)
          dx(1:3) = dv_b

          ! rotational kinematics equations
          call quaternion_derivative(q_nb, omega_nb_b, dq_nb)
          dx(4:7) = dq_nb

          ! moment equations
          m_b = matmul(c_bw, ma_w) + mt_b
          dx(8:10) = matmul(inv_j_b, &
                m_b - cross(omega_nb_b, matmul(j_b, omega_nb_b)))

          ! navigation equations: dp_n, dp_e, dh_d
          v_n = matmul(c_nb, v_b)
          dx(11:12) = v_n(1:2)
          dx(13) = -v_n(3)

          ! actuators: limit cmd to simulate saturation
          ! todo this may not be the best way, but avoids
          ! complicated issues with integration, also
          ! the plant is first order so there is no
          ! overshoot
          if (abs(delta_l_cmd) > elvn_max) then
                delta_l_cmd = sign(elvn_max, delta_l_cmd)
          end if
          if (abs(delta_r_cmd) > elvn_max) then
                delta_r_cmd = sign(elvn_max, delta_r_cmd)
          end if
          if (delta_t_cmd > 1) then
                delta_t_cmd = 1
          else if (delta_t_cmd < 0) then
                delta_t_cmd = 0
          end if
          dx(14) = (delta_l_cmd - delta_l)/delta_l_tau
          dx(15) = (delta_r_cmd - delta_r)/delta_r_tau
          dx(16) = (delta_t_cmd - delta_t)/delta_t_tau

          ! gyro bias, the gyro dynamics are stationary, but
          ! there is random noise
          dx(17:19) = 0.0d0

          ! should we do a measurement
          if (measure) then

          ! old measurement data for zero order hold
          ! first 6 measurements are just extra state data
          t_gps = y(7)
          y_v_n = y(8)
          y_v_e = y(9)
          y_v_d = y(10)
          y_p_n = y(11)
          y_p_e = y(12)
          y_h = y(13)
          t_mag = y(14)
          y_b_x = y(15)
          y_b_y = y(16)
          y_b_z = y(17)
          t_gyro = y(18)
          y_p = y(19)
          y_q = y(20)
          y_r = y(21)
          t_accel = y(22)
          y_a_x = y(23)
          y_a_y = y(24)
          y_a_z = y(25)
          t_baro = y(26)
          y_p_a = y(27)
          t_pitot = y(28)
          y_p_d = y(29)

          ! gps measurement
          call zero_order_hold(t, gps_period, t_gps, &
                y_update(i_gps))
          if (y_update(i_gps)) then
                t_gps = t
                y_v_n = adc(v_n(1) + gps_v_std*y_noise(1), &
                        -gps_v_max, gps_v_max, gps_v_res)
                y_v_e = adc(v_n(2) + gps_v_std*y_noise(2), &
                        -gps_v_max, gps_v_max, gps_v_res)
                y_v_d = adc(v_n(3) + gps_v_std*y_noise(3), &
                        -gps_v_max, gps_v_max, gps_v_res)
                y_p_n = adc(p_n + gps_p_std*y_noise(4), &
                        -gps_p_max, gps_p_max, gps_p_res)
                y_p_e = adc(p_e + gps_p_std*y_noise(5), &
                        -gps_p_max, gps_p_max, gps_p_res)
                y_h = adc(h + gps_h_std*y_noise(6), &
                        gps_h_min, gps_h_max, gps_h_res)
          end if

          ! baro measurement
          call zero_order_hold(t, baro_period, t_baro, &
                y_update(i_baro))
          if (y_update(i_baro)) then
                t_baro = t
                y_p_a = adc(0.0d0 + baro_std*y_noise(7), &
                        baro_min, baro_max, baro_res)
          end if

          ! pitot measurement
          call zero_order_hold(t, pitot_period, t_pitot, &
                y_update(i_pitot))
          if (y_update(i_pitot)) then
                t_pitot = t
                y_p_d = adc(0.0d0 + pitot_std*y_noise(8), &
                        pitot_min, pitot_max, pitot_res)
          end if

          ! gyro measurement
          call zero_order_hold(t, gyro_period, t_gyro, &
                y_update(i_gyro))
          if (y_update(i_gyro)) then
                t_gyro = t
                y_p = adc(p + gyro_bias_x + gyro_std*y_noise(9), &
                          -gyro_max, gyro_max, gyro_res)
                y_q = adc(q + gyro_bias_y + gyro_std*y_noise(10), &
                          -gyro_max, gyro_max, gyro_res)
                y_r = adc(r + gyro_bias_z + gyro_std*y_noise(11), &
                          -gyro_max, gyro_max, gyro_res)
          end if

          ! accel measurement
          call zero_order_hold(t, accel_period, t_accel, &
                y_update(i_accel))
          if (y_update(i_accel)) then
                t_accel = t
                accel_b = dv_b - matmul(c_bn, g_n)
                y_a_x = adc(accel_b(1) + accel_std*y_noise(12), &
                        -accel_max, accel_max, accel_res)
                y_a_y = adc(accel_b(2) + accel_std*y_noise(13), &
                        -accel_max, accel_max, accel_res)
                y_a_z = adc(accel_b(3) + accel_std*y_noise(14), &
                        -accel_max, accel_max, accel_res)
          end if

          ! mag measurement
          call zero_order_hold(t, mag_period, t_mag, &
                y_update(i_mag))
          if (y_update(i_mag)) then
                t_mag = t
                ! todo add mag dec/incl. to field/ params
                b_n = (/1, 0, 0/)
                b_b = matmul(c_bn, b_n)
                y_b_x = adc(b_b(1) + mag_std*y_noise(15), &
                        -mag_max, mag_max, mag_res)
                y_b_y = adc(b_b(2) + mag_std*y_noise(16), &
                        -mag_max, mag_max, mag_res)
                y_b_z = adc(b_b(3) + mag_std*y_noise(17), &
                        -mag_max, mag_max, mag_res)
          end if
          
          y = (/v_t, alpha, beta, phi, theta, psi, &
                t_gps, y_v_n, y_v_e, y_v_d, y_p_n, y_p_e, y_h, &
                t_mag, y_b_x, y_b_y, y_b_z, &
                t_gyro, y_p, y_q, y_r, &
                t_accel, y_a_x, y_a_y, y_a_z, &
                t_baro, y_p_a, &
                t_pitot, y_p_d /)

          ! measure end if
          end if 

      end subroutine dynamics

      pure function adc(u, min, max, resolution)
          real(8), intent(in) :: u, min, max, resolution
          real(8) :: adc
          real(8) :: f
          integer :: n
          if (u < min) then
                f = min
          else if (u > max) then
                f = max
          else
                f = u
          end if
          adc = n*int(f/resolution)
      end function adc

      pure subroutine zero_order_hold(t, period, time_stamp, update)
          real(8), intent(in) :: t
          real(8), intent(in) :: period
          real(8), intent(inout) :: time_stamp
          logical, intent(out) :: update
          update = .false.
          if (t - time_stamp >= period) then
                time_stamp = t
                update = .true.
          end if
      end subroutine zero_order_hold

      pure subroutine normalize_quaternions(x_vect)
          real(8), dimension(19), intent(inout) :: x_vect
          real(8), dimension(4) :: q
          q(1:4) = x_vect(4:7)
          x_vect(4:7) = q/norm2(q)
      end subroutine normalize_quaternions

      pure function cross(a, b)
          real(8), dimension(3) :: cross
          real(8), dimension(3), intent(in) :: a, b
          cross(1) = a(2) * b(3) - a(3) * b(2)
          cross(2) = a(3) * b(1) - a(1) * b(3)
          cross(3) = a(1) * b(2) - a(2) * b(1)
      end function cross

      pure subroutine compute_c_bw(alpha, beta, c_bw)
          ! wind to body dcm
          ! lewis pg. 73 (tranpose of)
          ! u_b = c_bw u_w
          real(8), intent(in) :: alpha, beta
          real(8), dimension(3,3), intent(out) :: c_bw
          real(8) :: calpha, cbeta, salpha, sbeta
          calpha = cos(alpha)
          cbeta = cos(beta)
          salpha = sin(alpha)
          sbeta = sin(beta)
          c_bw(1,1) = calpha*cbeta
          c_bw(1,2) = -calpha*sbeta
          c_bw(1,3) = -salpha
          c_bw(2,1) = sbeta
          c_bw(2,2) = cbeta
          c_bw(2,3) = 0.0d0
          c_bw(3,1) = salpha*cbeta
          c_bw(3,2) = -salpha*sbeta
          c_bw(3,3) = calpha
      end subroutine compute_c_bw
    
      pure subroutine dcm_to_euler(c_nb, phi, theta, psi)
          ! titterton pg. 46
          real(8), dimension(3,3), intent(in) :: c_nb
          real(8), intent(out) :: phi, theta, psi
          phi = atan2(c_nb(3,2), c_nb(3,3))
          theta = asin(-c_nb(3,1))
          psi = atan2(c_nb(2,1), c_nb(1,1))
      end subroutine dcm_to_euler

      pure subroutine quaternion_to_euler(q_nb, phi, theta, psi)
          ! u_n = c_nb u_b
          real(8), dimension(4), intent(in) :: q_nb
          real(8), intent(out) :: phi, theta, psi
          real(8), dimension(3,3) :: c_nb
          call quaternion_to_dcm(q_nb, c_nb)
          call dcm_to_euler(c_nb, phi, theta, psi)
      end subroutine quaternion_to_euler

      pure subroutine euler_to_dcm(phi, theta, psi, c_nb)
          ! u_n = c_nb u_b
          ! euler (body 321)
          ! u_n = psi(3) <- theta(2) <- phi(1) u_b
          real(8), intent(in) :: phi, theta, psi
          real(8), dimension(3,3), intent(out) :: c_nb
          real(8) :: cphi, ctheta, cpsi, sphi, stheta, spsi
          cphi = cos(phi)
          ctheta = cos(theta)
          cpsi = cos(psi)
          sphi = sin(phi)
          stheta = sin(theta)
          spsi = sin(psi)
          c_nb(1,1) = ctheta*cpsi 
          c_nb(1,2) = -cphi*spsi + sphi*stheta*cpsi
          c_nb(1,3) = sphi*spsi + cphi*stheta*cpsi
          c_nb(2,1) = ctheta*spsi
          c_nb(2,2) = cphi*cpsi + sphi*stheta*spsi
          c_nb(2,3) = -sphi*cpsi + cphi*stheta*spsi
          c_nb(3,1) = -stheta
          c_nb(3,2) = sphi*ctheta
          c_nb(3,3) = cphi*ctheta
      end subroutine euler_to_dcm

      pure subroutine quaternion_to_dcm(q_nb, c_nb)
          ! titterton pg. 45
          ! u_n = c_nb u_b
          real(8), dimension(4), intent(in) :: q_nb
          real(8), dimension(3,3), intent(out) :: c_nb
          real(8) :: a, b, c, d, aa, bb, cc, dd
          a = q_nb(1)
          b = q_nb(2)
          c = q_nb(3)
          d = q_nb(4)
          aa = a*a
          bb = b*b
          cc = c*c
          dd = d*d
          c_nb(1,1) = aa + bb - cc - dd
          c_nb(1,2) = 2*(b*c - a*d)
          c_nb(1,3) = 2*(b*d + a*c)
          c_nb(2,1) = 2*(b*c + a*d)
          c_nb(2,2) = aa - bb + cc - dd
          c_nb(2,3) = 2*(c*d - a*b)
          c_nb(3,1) = 2*(b*d - a*c)
          c_nb(3,2) = 2*(c*d + a*b)
          c_nb(3,3) = aa - bb - cc + dd
      end subroutine quaternion_to_dcm

      pure subroutine quaternion_derivative(q_nb, omega_nb_b, dq_nb)
          ! titterton pg. 44
          ! u_n = c_nb u_b
          real(8), dimension(4), intent(in) :: q_nb
          real(8), dimension(3), intent(in) :: omega_nb_b
          real(8), dimension(4), intent(out) :: dq_nb
          real(8) :: a, b, c, d, wx, wy, wz
          a = q_nb(1)
          b = q_nb(2)
          c = q_nb(3)
          d = q_nb(4)
          wx = omega_nb_b(1)
          wy = omega_nb_b(2)
          wz = omega_nb_b(3)
          dq_nb(1) = -0.5d0*(b*wx + c*wy + d*wz)
          dq_nb(2) = 0.5d0*(a*wx - d*wy + c*wz)
          dq_nb(3) = 0.5d0*(d*wx + a*wy - b*wz)
          dq_nb(4) = -0.5d0*(c*wx - b*wy - a*wz)
      end subroutine quaternion_derivative

      pure subroutine euler_to_quaternion(phi, theta, psi, q_nb)
          ! u_n = c_nb u_b
          ! euler (body 321)
          ! u_n = psi(3) <- theta(2) <- phi(1) u_b
          real(8), intent(in) :: phi, theta, psi
          real(8), dimension(4), intent(out) :: q_nb
          real(8) :: cphi2, ctheta2, cpsi2, sphi2, stheta2, spsi2
          cphi2 = cos(phi/2.0d0)
          ctheta2 = cos(theta/2.0d0)
          cpsi2 = cos(psi/2.0d0)
          sphi2 = sin(phi/2.0d0)
          stheta2 = sin(theta/2.0d0)
          spsi2 = sin(psi/2.0d0)
          q_nb(1) = cphi2*ctheta2*cpsi2 + sphi2*stheta2*spsi2
          q_nb(2) = sphi2*stheta2*cpsi2 - cphi2*stheta2*spsi2
          q_nb(3) = cphi2*stheta2*cpsi2 + sphi2*ctheta2*spsi2
          q_nb(4) = cphi2*ctheta2*spsi2 - sphi2*stheta2*cpsi2
          ! + changed to - (typo in book) for q_nb(4)
      end subroutine euler_to_quaternion

      end module camflyer
