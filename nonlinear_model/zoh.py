class ZeroOrderHold(object):
    """
    This class models a zero order hold block.
    The update function takes the time and a callback function
    to update the state of the block. The function is called
    if the ZOH period has elapsed.
    """
    def __init__(self, period):
        self.period = period
        self.t = None
        self.x = None
        self.updated = False
    def update(self, t, f):
        if self.t is None or (t - self.t >= self.period):
            self.t = t
            self.x = f()
            self.updated = True
        return self.x
