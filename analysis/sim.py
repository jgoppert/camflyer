import numpy as np
import scipy.integrate
import scipy.optimize
import matplotlib.pyplot as plt


def simulate(f_sim, f_cont,
             x0, u0, dt=1.0/50.0, tf=10):
    # Closed Loop Simulation

    sim = scipy.integrate.ode(f_sim)
    sim.set_integrator('dopri5')

    # x0_stddev = 0.1
    process_noise_stddev = 0  # TODO fix process noise sampling

    sim.set_initial_value(x0)

    n_t = np.floor(tf/dt)
    n_x = len(x0)
    n_u = len(u0)

    class Data(object):
        def __init__(self):
            self.t = np.zeros(n_t)
            self.y = np.zeros((n_t, n_x))
            self.u = np.zeros((n_t, n_u))

        def truncate(self, i):
            self.t = self.t[:i]
            self.y = self.y[:i, :]
            self.u = self.u[:i, :]
    data = Data()

    i = 0
    while i < n_t:

        x = sim.y
        u = f_cont(x)

        sim.set_f_params(u.T)
        sim.set_initial_value(
            sim.y + process_noise_stddev*np.random.randn(len(x)), sim.t)
        sim.integrate(sim.t + dt)
        if not sim.successful():
            print 'failed at time: %f' % sim.t
            data.truncate(i)
            break
        data.y[i, :] = sim.y
        data.t[i] = sim.t
        data.u[i, :] = u
        i += 1

    return data


def plot_control_history(name, data, x0, u0):

    color_cycle = ['b', 'g', 'r', 'c', 'm',
                   'y', 'k', 'orange', 'navy', 'grey']
    plt.subplot(121)
    plt.gca().set_color_cycle(color_cycle)
    h = plt.plot(data.t, data.y - x0, linewidth=5)
    plt.legend(h, ['$\\alpha$', '$Q$', '$V_T$', '$\\theta$', '$h$',
                   '$\\beta$', '$\\phi$', '$P$', '$R$', '$\\psi$'], loc='best',
               ncol=2)
    plt.title('trim simulation')
    plt.title('state error')
    plt.xlabel('t, sec')
    plt.ylabel('$x - x_0$')
    plt.grid()

    plt.subplot(122)
    plt.gca().set_color_cycle(color_cycle)
    h = plt.plot(data.t, data.u - u0, linewidth=5)
    plt.legend(h, ['elevator', 'throttle', 'aileron'], loc='best',
               ncol=2)
    plt.title('control effort')
    plt.xlabel('t, sec')
    plt.ylabel('$u - u_0$')
    plt.grid()
