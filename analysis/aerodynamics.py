import matplotlib.pyplot as plt
import os
import string
import datetime
import numpy

import pyxflr


def generate_plots_and_tables(
        aerodynamics, fig_path='figures', latex_path='latex'):

    polar = aerodynamics['polar']
    stab = aerodynamics['stab']

    # setup output directories
    for path in [fig_path, latex_path]:
        if not os.path.exists(path):
            print 'creating path: ', path
            os.makedirs(path)

    # plot aero data
    print('plotting data')
    figs = pyxflr.plot_polar(polar)
    for fig_name in figs.keys():
        plt.figure(figs[fig_name].number)
        plt.savefig(os.path.join('figures', fig_name+'.pdf'))

    # write tables
    latex_table = string.Template(r'''\begin{tabular}{$columns}
    $header\\
    \hline
    $contents\end{tabular}''')

    # write stability derivs
    with open(os.path.join(latex_path, 'tbl_aero_stab.tex'), 'w') as aero_file:
        contents = ''
        data_dict = stab['stability derivs']
        for key in sorted(data_dict.keys()):
            contents = contents + '{:s} & {:f} \\\\\n'.format(
                key,
                data_dict[key]['val'])
        aero_file.write(latex_table.substitute(
            header='coefficient & value',
            columns='c|c',
            contents=contents))

    # write general aero
    with open(os.path.join(latex_path, 'tbl_aero_gen.tex'), 'w') as aero_file:
        contents = ''
        data_dict = stab['general']
        for key in sorted(data_dict.keys()):
            contents = contents + '{:s} & {:f} & {:s} \\\\\n'.format(
                key,
                data_dict[key]['val'], data_dict[key]['unit'])
        aero_file.write(latex_table.substitute(
            header='coefficient & value & unit',
            columns='c|c|c',
            contents=contents))

    with open(os.path.join(latex_path, 'out.stamp'), 'w') as stamp_file:
        stamp_file.write(datetime.datetime.now().strftime(
            '%Y-%m-%d %H:%M:%S'))


def fit(xflr_polar=os.path.join('data', 'T1-9.0 m s-VLM1.csv'),
        xflr_stability=os.path.join('data', 'stability-analysis.txt')):

    # read xflr5 aero data
    print('reading xflr4 data')
    polar = pyxflr.read_polar_csv(xflr_polar)
    stab = pyxflr.read_stability(xflr_stability)

    alpha_deg = polar['alpha']
    alpha_rad = numpy.deg2rad(alpha_deg)
    CL = polar['CL']
    CD = polar['CD']

    coeff = {}

    # CL vs alpha
    CL_alpha_fit = numpy.polyfit(alpha_rad, CL, 1, full=True)
    CL_alpha_fit_Rsq = 1 - CL_alpha_fit[1] / (CL.size * CL.var())
    a, b = CL_alpha_fit[0]
    coeff['CL_alpha'] = a
    coeff['CL_0'] = b
    coeff['CL_alpha_fit_Rsq'] = CL_alpha_fit_Rsq[0]

    # CD vs alpha
    CD_alpha_fit = numpy.polyfit(alpha_rad, CD, 2, full=True)
    CD_alpha_fit_Rsq = 1 - CD_alpha_fit[1] / (CD.size * CD.var())
    a, b, c = CD_alpha_fit[0]
    coeff['alpha_DM'] = -b/(2*a)
    coeff['CD_DM'] = c - b**2/(4*a)
    coeff['k_CD_CL'] = a
    coeff['CD_alpha_fit_Rsq'] = CD_alpha_fit_Rsq[0]

    # set stability derivatives
    sd = stab['stability derivs']
    coeff['Cl_beta'] = sd['Clb']['val']
    coeff['Cl_p'] = sd['Clp']['val']
    coeff['Cm_q'] = sd['Cmq']['val']
    coeff['Cn_beta'] = sd['Cnb']['val']
    coeff['Cn_r'] = sd['Cnr']['val']

    return {'polar': polar, 'stability': stab, 'coeff': coeff}


def plot_fits(aerodynamics):
    polar = aerodynamics['polar']
    coeff = aerodynamics['coeff']
    alpha_deg = polar['alpha']
    CL = polar['CL']
    CD = polar['CD']
    alpha_x_deg = numpy.linspace(min(alpha_deg), max(alpha_deg))
    alpha_x_rad = numpy.deg2rad(alpha_x_deg)

    # CL vs alpha
    plt.figure()
    plt.plot(alpha_deg, CL, 'ro')
    plt.plot(alpha_x_deg, coeff['CL_alpha']*alpha_x_rad +
             coeff['CL_0'], 'k-'),
    plt.title('CL vs $\\alpha$')
    plt.xlabel('$\\alpha$, deg')
    plt.ylabel('CL')
    plt.annotate('$R^2$ = %f\n$%f x + %f=0$' % (
                 coeff['CL_alpha_fit_Rsq'],
                 coeff['CL_alpha'],
                 coeff['CL_0']),
                 xy=(0.05, 0.8), xycoords='axes fraction')

    # CD vs alpha
    plt.figure()
    plt.plot(alpha_deg, CD, 'ro')
    plt.plot(alpha_x_deg,
             coeff['k_CD_CL'] *
             (alpha_x_rad - coeff['alpha_DM'])**2 +
             coeff['CD_DM'], 'k-')
    plt.title('CD vs. $\\alpha$')
    plt.xlabel('$\\alpha$, deg')
    plt.ylabel('CD')
    plt.annotate('$R^2$ = %f\n$%f (x - %f)^2 + %f=0$' % (
                 coeff['CD_alpha_fit_Rsq'],
                 coeff['k_CD_CL'],
                 coeff['alpha_DM'],
                 coeff['CD_DM']),
                 xy=(0.05, 0.8), xycoords='axes fraction')

# vim:ts=4:sw=4:expandtab
