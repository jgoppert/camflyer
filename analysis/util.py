import pickle
import os
import argparse

from . import aerodynamics
from . import constants
from . import trim
from . import dynamics
from . import dynamic_inversion


def load_or_create(filename, create_func, rerun=False):

    def create():
        print('creating %s' % filename)
        data = create_func()
        with open(filename, 'w') as f:
            pickle.dump(data, f)
        return data

    def load():
        with open(filename, 'r') as f:
            data = pickle.load(f)
        return data

    if (rerun):
        data = create()
    else:
        try:
            data = load()
            with open(filename, 'r') as f:
                data = pickle.load(f)
        except Exception:
            data = create()

    return data


def load_or_create_all(save_path='pickle',
                       rerun_aerodynamics=False,
                       rerun_dynamics=False,
                       rerun_trim=False,
                       rerun_inversion=False):

    data = {}

    # create save path if it doesn't exist
    if not os.path.exists(save_path):
            os.makedirs(save_path)

    # load aerodynamics
    data['aerodynamics'] = load_or_create(
        os.path.join('pickle', 'aerodynamics'),
        lambda: aerodynamics.fit(),
        rerun=rerun_aerodynamics)

    # read cfd data and create polynomial constants
    # aerodynamics = analysis.aerodynamics()
    data['constants'] = constants.constants

    # derive dynamics of aircraft
    data['dynamics'] = load_or_create(
        os.path.join('pickle', 'dynamics'),
        lambda: dynamics.derive(),
        rerun=rerun_dynamics)

    # trim aircraft
    data['trim'] = load_or_create(
        os.path.join('pickle', 'trim'),
        lambda: trim.trim(data['dynamics'], data['constants']),
        rerun=rerun_trim)

    # dynamic inversion controller
    data['inversion'] = load_or_create(
        os.path.join('pickle', 'inversion'),
        lambda: dynamic_inversion.invert_dynamics(data['dynamics']),
        rerun=rerun_inversion)

    # design controller
    # lqr_design(trim)

    return data


def load_or_create_all_parse(argv):

    parser = argparse.ArgumentParser(description='main analysis')
    parser.add_argument('-aerodynamics_rerun',
                        action='store_true', default=False)
    parser.add_argument('-dynamics_rerun',
                        action='store_true', default=False)
    parser.add_argument('-inversion_rerun',
                        action='store_true', default=False)
    parser.add_argument('-trim_rerun',
                        action='store_true', default=False)

    args = parser.parse_args(argv[1:])

    return load_or_create_all(
        rerun_aerodynamics=args.aerodynamics_rerun,
        rerun_dynamics=args.dynamics_rerun,
        rerun_inversion=args.inversion_rerun,
        rerun_trim=args.trim_rerun
    )
