import sympy
import sympy.physics.mechanics as mech


def derive():

    print('defining variables')
    alpha_e, rho, S, c_bar, b, J_x, J_y, J_z, J_xz, m, t, g_D, gam =\
        sympy.symbols('alpha_e, rho, S, \\bar{c}, b, J_x, J_y, J_z, '
                      'J_xz, m, t, g_D, gamma')
    phi, theta, psi, h, lat, lon, alt, alpha, beta, V_T,\
        P, Q, R, w_X, w_Y, w_Z =\
        sympy.symbols('phi, theta, psi, h, lon, lat, alt, '
                      'alpha, beta, V_T, P, Q, R, w_X, w_Y, w_Z')
    thrust, CL, CD, CC, Cl, Cm, Cn =\
        sympy.symbols('thrust, C_L, C_D, C_C, C_l, C_m, C_n')
    delta_t, delta_a, delta_e, delta_r =\
        sympy.symbols('delta_t, delta_a, delta_e, delta_r')

    print('defining frames')
    frame_i = mech.ReferenceFrame('i')
    frame_e = frame_i.orientnew('e', 'Axis', (0, frame_i.z))
    frame_n = frame_e.orientnew('n', 'Body', (-lon(t), lat(t), 0), '321')
    frame_b = frame_n.orientnew('b', 'Body', (psi(t), theta(t), phi(t)), '321')
    # frame_s = frame_b.orientnew('s', 'Axis', (-alpha_e, frame_b.y))
    frame_w = frame_b.orientnew('w', 'Body', (-alpha(t), beta(t), 0), '231')

    print('solving for euler/body rates relationship')
    omega_bn = frame_b.ang_vel_in(frame_n)
    euler_rates = sympy.solve([
        P - omega_bn.dot(frame_b.x),
        Q - omega_bn.dot(frame_b.y),
        R - omega_bn.dot(frame_b.z)],
        [phi(t).diff(t), theta(t).diff(t), psi(t).diff(t)])
    # substitute P,Q,R as rates instead of based on euler angles, since states
    frame_b.set_ang_vel(frame_n,
                        P(t)*frame_b.x + Q(t)*frame_b.y + R(t)*frame_b.z)
    phi_dot_eq = euler_rates[phi(t).diff(t)]
    sympy.Eq(phi(t).diff(t), phi_dot_eq)
    theta_dot_eq = euler_rates[theta(t).diff(t)]
    sympy.Eq(theta(t).diff(t), theta_dot_eq)
    psi_dot_eq = euler_rates[psi(t).diff(t)]
    sympy.Eq(psi(t).diff(t), psi_dot_eq)

    print('solving for rigid body equations of motion')
    q_bar = rho*V_T(t)**2/2
    L = q_bar*S*CL(alpha)
    D = q_bar*S*CD(alpha)
    C = q_bar*S*CC(beta)
    l_w = q_bar*S*b*Cl(beta, delta_a, P)
    m_w = q_bar*S*c_bar*Cm(alpha, delta_e, Q)
    n_w = q_bar*S*b*Cn(beta, delta_r, R)

    J_b = mech.inertia(frame_b, J_x, J_y, J_z, 0, 0, J_xz)
    point_cm = mech.Point('b')
    point_cm.set_vel(frame_n, V_T(t)*frame_w.x)
    aircraft = mech.RigidBody('aircraft', point_cm,
                              frame_b, m, (J_b, point_cm))
    F_A = -D*frame_w.x - C*frame_w.y - L*frame_w.z
    F_W = m*g_D*frame_n.z
    F_T = thrust(delta_t)*frame_b.x
    F = (F_A + F_W + F_T).express(frame_w).simplify()

    aircraft_L = aircraft.linear_momentum(frame_n)
    eom_trans = aircraft_L.diff(t, frame_w) +\
        frame_w.ang_vel_in(frame_n).cross(aircraft_L) - F

    print('solving for explicit wind frame state equations')
    V_T_dot_eq = sympy.solve(eom_trans.dot(frame_w.x), V_T(t).diff(t))[0]
    beta_dot_eq = sympy.solve(eom_trans.dot(frame_w.y), beta(t).diff(t))[0]
    alpha_dot_eq = sympy.solve(eom_trans.dot(frame_w.z), alpha(t).diff(t))[0]
    aircraft_H = aircraft.angular_momentum(point_cm, frame_n)
    M = l_w*frame_w.x + m_w*frame_w.y + n_w*frame_w.z
    eom_rot = aircraft_H.diff(t, frame_b) +\
        frame_b.ang_vel_in(frame_n).cross(aircraft_H) - M
    Q_dot_eq = sympy.solve(eom_rot.dot(frame_b.y), Q(t).diff(t))[0]
    # eom_rot_2 = eom_rot.subs(Q(t).diff(t), Q_dot_eq).simplify()
    rot_sol = sympy.solve([eom_rot.dot(frame_b.x), eom_rot.dot(frame_b.z)],
                          [P(t).diff(t), R(t).diff(t)], simplify=False)
    P_dot_eq = rot_sol[P(t).diff(t)].expand().factor()
    R_dot_eq = rot_sol[R(t).diff(t)].expand().factor()

    h_dot_eq = V_T*sympy.sin(theta(t) - alpha(t))

    t_sub = {V_T(t): V_T, h(t): h, alpha(t): alpha, beta(t): beta,
             P(t): P, Q(t): Q, R(t): R, phi(t): phi,
             theta(t): theta, psi(t): psi}
    gamma_eq = J_x*J_z - J_xz**2

    x_vect = sympy.Matrix([alpha, Q, V_T, theta, h,
                           beta, phi, P, R, psi]).subs(t_sub)
    u_vect = sympy.Matrix([delta_e, delta_t, delta_a, delta_r])

    x_i = {'alpha': 0, 'Q': 1, 'V_T': 2, 'theta': 3, 'h': 4,
           'beta': 5, 'phi': 6, 'P': 7, 'R': 8, 'psi': 9}
    x = sympy.DeferredVector('x')
    x_sub = {}
    for key in x_i:
        x_sub[key] = x[x_i[key]]

    u_i = {'elevator': 0, 'aileron': 1,
           'rudder': 2, 'throttle': 3}
    u = sympy.DeferredVector('u')
    u_sub = {}
    for key in u_i:
        u_sub[key] = u[u_i[key]]

    f_vect = sympy.Matrix([
        alpha_dot_eq, Q_dot_eq, V_T_dot_eq, theta_dot_eq, h_dot_eq,
        beta_dot_eq, phi_dot_eq, P_dot_eq, R_dot_eq, psi_dot_eq])
    f_vect = f_vect.subs(t_sub).subs({gamma_eq: gam, gamma_eq*2: gam*2})

    print('generic linearization')
    A = f_vect.jacobian(x_vect).\
        applyfunc(lambda expr: expr.factor().trigsimp())
    B = f_vect.jacobian(u_vect).\
        applyfunc(lambda expr: expr.factor().trigsimp())

    print('polynomial coefficient linearization')
    CL_0, CL_alpha, CC_beta, alpha_DM, CD_DM, k_CD_CL =\
        sympy.symbols('CL_0, CL_alpha, CC_beta, alpha_DM, CD_DM, k_CD_CL')
    Cl_beta, Cl_delta_a, Cm_delta_e, Cm_alpha, Cm_0 =\
        sympy.symbols('Cl_beta, Cl_delta_a, Cm_delta_e, Cm_alpha, Cm_0')
    Cl_p, Cm_q, Cn_r, Cn_beta, Cn_delta_r, thrust_max =\
        sympy.symbols('Cl_p, Cm_q, Cn_r, Cn_beta, Cn_delta_r, T_max')
    polars = {
        CD(alpha): CD_DM + k_CD_CL*(alpha - alpha_DM)**2,
        CL(alpha): CL_0 + CL_alpha*alpha,
        CC(beta): CC_beta*beta,
        Cl(beta, delta_a, P): Cl_beta*beta + Cl_delta_a*delta_a + Cl_p*P,
        Cm(alpha, delta_e, Q):
        Cm_0 + Cm_alpha*alpha + Cm_delta_e*delta_e + Cm_q*Q,
        Cn(beta, delta_r, R): Cn_beta*beta + Cn_delta_r*delta_r + Cn_r*R,
        thrust(delta_t): delta_t*thrust_max,
    }
    A_polars = A.subs(polars).applyfunc(lambda expr: expr.doit())
    B_polars = B.subs(polars).applyfunc(lambda expr: expr.doit())

    print('steady level flight linearization')
    x0_steady_level = {beta: 0, P: 0, Q: 0, R: 0, phi: 0, h: 0,
                       delta_a: 0, delta_r: 0, theta: alpha}
    A_steady_level = A_polars.subs(x0_steady_level).\
        applyfunc(lambda expr: expr.collect([k_CD_CL], sympy.factor))
    B_steady_level = B_polars.subs(x0_steady_level)

    print('checking linearized system is decoupled for steady level flight')
    assert(A_steady_level[0:5, 5:10].norm() == 0)
    assert(A_steady_level[5:10, 0:5].norm() == 0)
    assert(B_steady_level[0:5, 2:4].norm() == 0)
    assert(B_steady_level[5:10, 0:2].norm() == 0)

    def ctrb(A, B, n=B.shape[0]):
        CO = B
        for i in range(1, n):
            CO = CO.row_join(A**i*B)
        return CO

    print('checking controlability of linear system')
    CO = ctrb(A_steady_level, B_steady_level)
    assert(CO.rank() == 10)

    print('creating longitudinal subsystem')
    A_lon = A_steady_level[0:5, 0:5].\
        applyfunc(lambda expr: expr.simplify().
                  collect([k_CD_CL], sympy.factor))
    B_lon = B_steady_level[0:5, 0:2].\
        applyfunc(lambda expr: expr.simplify().
                  collect([k_CD_CL], sympy.factor))
    CO_lon = ctrb(A_lon, B_lon)
    assert(CO_lon.rank() == 5)

    print('creating lateral subsystem')
    A_lat = A_steady_level[5:10, 5:10].\
        applyfunc(lambda expr: expr.expand().
                  collect([Cl_beta, Cm_0, Cm_alpha, Cm_delta_e], sympy.factor))
    B_lat = B_steady_level[5:10, 2:4].\
        applyfunc(lambda expr: expr.expand().
                  collect([Cl_beta, Cm_0, Cm_alpha, Cm_delta_e], sympy.factor))
    CO_lat = ctrb(A_lat, B_lat)
    assert(CO_lat.rank() == 5)

    print('combining dynamics with polars')
    f_vect_polars = f_vect.subs(polars).applyfunc(lambda expr: expr.doit())

    print('storing expressions')
    save_dict = {}
    for key in 'A_lon B_lon x_vect u_vect x_i x_sub u_i u_sub'\
               ' A_lat B_lat f_vect_polars gamma_eq'.split():
        save_dict[key] = locals()[key]
    return save_dict
