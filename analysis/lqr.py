import numpy as np
import control


def cont_lqr_design(A, B, Q, R):
    X, L, G = control.care(A, B, Q, R)
    return control.ss(
        np.zeros((0, 0)),
        np.zeros((0, Q.shape[0])),
        np.zeros((R.shape[0], 0)), G)


# def discr_lqr_design(A, B, Q, R):
#     X, L, G = control.dare(A, B, Q, R)
#     return control.ss(
#         np.zeros((0, 0)),
#         np.zeros((0, Q.shape[0])),
#         np.zeros((R.shape[0], 0)), G)


def linear_control_law(x, x0, u0, K):
    dx = x0 - x
    du = K.dot(dx)
    u = u0 + du
    # saturate
    u = np.where(u < -1, -1, u)
    u = np.where(u > 1, 1, u)
    return (u0 + du)


def lqr_analysis(ss_lon_closed, ss_lat_closed):
    n_x = ss_lon_closed.B.shape[0]
    n_u = ss_lon_closed.B.shape[1]
    control.bode([ss_lon_closed[i, j]
                 for i in range(n_x) for j in range(n_u)],
                 omega=np.logspace(-2, 2))
    print 'longitudinal subsystem'
    print 'gain margin,  phase margin, W-gain-crossover, W-phase-crossover'
    for i in range(4):
        for j in range(2):
            print control.margin(ss_lon_closed[i, j])

    np.linalg.eig(ss_lon_closed.A)

    print 'lateral subsystem'
    print('closed loop eigenvalues')
    print(np.linalg.eig(ss_lat_closed.A))

    control.bode([ss_lat_closed[i, j]
                 for i in range(2) for j in range(2)],
                 omega=np.logspace(-2, 2))

    print('gain margin,  phase margin, W-gain-crossover, W-phase-crossover')
    for i in range(4):
        for j in range(2):
            print control.margin(ss_lat_closed[i, j])


def lqr_design(trim, Q_lon, R_lon,
               Q_lat, R_lat):
    ss_lon = trim['ss_lon']
    ss_lat = trim['ss_lat']

    # Control Design

    # Longitudinal
    C_lon_lqr = cont_lqr_design(
        A=ss_lon.A,
        B=ss_lon.B, Q=Q_lon, R=R_lon)
    ss_lon_closed = ss_lon.feedback(C_lon_lqr)

    # Lateral
    C_lat_lqr = cont_lqr_design(
        A=ss_lat.A,
        B=ss_lat.B, Q=Q_lat, R=R_lat)
    ss_lat_closed = ss_lat.feedback(C_lat_lqr)

    n_x_lon = ss_lon.B.shape[0]
    n_x_lat = ss_lat.B.shape[0]
    n_x = n_x_lon + n_x_lat

    n_u_lon = ss_lon.B.shape[1]
    n_u_lat = ss_lat.B.shape[1]
    n_u = n_u_lon + n_u_lat

    K = np.zeros((n_u, n_x))
    K[0:n_u_lon, 0:n_x_lon] = C_lon_lqr.D
    K[n_u_lon:n_u, n_x_lon:n_x] = C_lat_lqr.D

    return K, ss_lon_closed, ss_lat_closed
