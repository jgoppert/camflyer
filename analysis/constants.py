import sympy

# all of these should be read from xflr5 file, but some
# haven't been handled yet

constants = {
    # environment
    'g_D': 9.8,
    'rho': 1.225,
    # mass
    'CoG_x': 0.1285,
    'CoG_z': 0.00007443,
    'J_x': 1.744e-2,
    'J_y': 2.538e-2,
    'J_z': 4.281e-2,
    'J_xz': 59.7e-6,
    'm': 0.3,
    # propulsion
    'T_max': 20,
    # aerodynamics
    sympy.Symbol(r'\bar{c}'): 0.20853,
    'S': 0.17188,
    'b': 0.846,
    'k_CD_CL': 1,
    'CL_0': 0.1,
    'CL_alpha': 0.1,
    'Cl_beta': -0.1,
    'Cl_delta_a': 0.1,
    'Cl_p': -0.1,
    'Cm_0': 0.1,
    'Cm_alpha': -0.1,
    'Cm_delta_e': 0.1,
    'Cm_q': -1,
    'alpha_DM': 0.1,
    'CD_DM': 0.05,
    'CC_beta': -0.1,
    'Cn_beta': -0.1,
    'Cn_r': -0.1,
    'Cn_delta_r': 0.1,
}
constants['gamma'] = constants['J_x']*constants['J_z'] \
    - constants['J_xz']**2
