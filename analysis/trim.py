# trim aircraft

import sympy
import numpy as np
import scipy.integrate
import scipy.optimize
import matplotlib.pyplot as plt
import control


def c2d(sys, dt):
    dsys = scipy.signal.cont2discrete((sys.A, sys.B, sys.C, sys.D), dt)
    return control.ss(dsys[0], dsys[1], dsys[2], dsys[3], dt)


def create_f_sim(f, constants):
    from sympy import Symbol
    x = sympy.DeferredVector('x')
    u = sympy.DeferredVector('u')
    f_tmp = f.subs(constants).subs({
        'alpha': x[0],
        'Q': x[1],
        'V_T': x[2],
        'theta': x[3],
        'h': x[4],
        'beta': x[5],
        'phi': x[6],
        'P': x[7],
        'R': x[8],
        'psi': x[9],
        'delta_e': u[0],
        'delta_t': u[1],
        'delta_a': u[2],
        'delta_r': 0,
    })
    return sympy.lambdify((Symbol('t'), x, u), f_tmp)


def simulate(f_sim, x0, u0, dt=0.1, tf=10):
    # Simulation of Trim
    sim = scipy.integrate.ode(f_sim)
    sim.set_integrator('dopri5')
    sim.set_initial_value(x0)
    n_t = np.floor(tf/dt)
    n_x = len(x0)
    y = np.zeros((n_t, n_x))
    t = np.zeros(n_t)
    i = 0
    while i < n_t:
        sim.set_f_params(u0)
        sim.integrate(sim.t + dt)
        if not sim.successful():
            print 'failed at time: %f' % sim.t
            y = y[:i, :]
            t = t[:i]
            print i
            break
        y[i, :] = sim.y
        t[i] = sim.t
        i += 1
    color_cycle = ['b', 'g', 'r', 'c', 'm',
                   'y', 'k', 'orange', 'navy', 'grey']
    plt.gca().set_color_cycle(color_cycle)
    h = plt.plot(t, y, linewidth=5)
    plt.legend(h, ['$\\alpha$', '$Q$', '$V_T$', '$\\theta$', '$h$',
                   '$\\beta$', '$\\phi$', '$P$', '$R$', '$\\psi$'], loc='best',
               ncol=2)
    plt.title('trim simulation')


def create_state_space(A, B, x0, u0, constants):
    x0_sub = {'alpha': x0[0], 'Q': x0[1], 'V_T': x0[2],
              'theta': x0[3], 'h': x0[4],
              'beta': x0[5], 'phi': x0[6], 'P': x0[7], 'R': x0[8],
              'psi': x0[9]}
    u0_sub = {'delta_e': u0[0], 'delta_t': u0[1], 'delta_a': u0[2]}
    A0 = np.array(A.subs(constants).subs(x0_sub).subs(u0_sub)).astype(float)
    B0 = np.array(B.subs(constants).subs(x0_sub).subs(u0_sub)).astype(float)
    n_x = B0.shape[0]
    n_u = B0.shape[1]
    ss = control.ss(A0, B0, np.eye(n_x), np.zeros((n_x, n_u)))
    return ss


def analyze_state_space(ss):
    c2d(ss, 0.01)
    print np.linalg.eigvals(ss.A)
    control.bode([ss[i, j]
                 for i in range(4) for j in range(2)],
                 omega=np.logspace(-2, 2))
    print np.linalg.matrix_rank(
        control.ctrb(ss.A, ss.B))
    print np.linalg.cond(
        control.ctrb(ss.A, ss.B))


def trim_steady_level(f_sim, V_T):
    # Trimming the Aircraft
    P = 0.0
    Q = 0.0
    R = 0.0
    beta = 0.0
    phi = 0.0
    h = 0.0
    psi = 0.0

    def cost(xd, get_trim=False):
        alpha = xd[0]
        theta = alpha  # flight path angle zero
        delta_e = xd[1]
        delta_t = xd[2]
        delta_a = xd[3]
        x0 = [alpha, Q, V_T, theta, h, beta, phi, P, R, psi]
        u0 = [delta_e, delta_t, delta_a]
        dx = f_sim(0, x0, u0)
        if get_trim:
            return x0, u0
        else:
            s = 0
            for i in [0, 1, 2, 3, 5, 6, 7, 8]:
                s += dx[i]*dx[i]
            return s

    xs = scipy.optimize.fmin(cost, [0.0, 0.0, 0.0, 0.0])
    x0, u0 = cost(xs, get_trim=True)
    return x0, u0


def trim(dynamics, constants):

    A_lat = dynamics['A_lat']
    B_lat = dynamics['B_lat']
    A_lon = dynamics['A_lon']
    B_lon = dynamics['B_lon']
    f_vect_polars = dynamics['f_vect_polars']

    print('creating trim function')
    f_sim = create_f_sim(f_vect_polars, constants)

    print('trimming')
    x0, u0 = trim_steady_level(f_sim, 10.0)

    print('creating state space models')
    # note, not including rudder input in B_lat
    # since camflyer doesn't have it
    ss_lat = create_state_space(A_lat, B_lat[:, 1], x0, u0, constants)
    ss_lon = create_state_space(A_lon, B_lon, x0, u0, constants)

    return {'ss_lat': ss_lat, 'ss_lon': ss_lon,
            'x0': x0, 'u0': u0}
