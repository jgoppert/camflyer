import sympy
import numpy as np
import sys


def invert_dynamics(dynamics):

    f_vect_polars = dynamics['f_vect_polars']
    P_dot, Q_dot, R_dot, psi_dot, h_dot, VT_dot, t, delta_a,\
        delta_t, delta_e, delta_r =\
        sympy.symbols('P_dot, Q_dot, R_dot, psi_dot, h_dot, VT_dot, t, '
                      'delta_a, delta_t, delta_e, delta_r')
    x_i = dynamics['x_i']

    print 'inverting attitude dynamics'
    sys.stdout.flush()
    invert_sol_att = sympy.solve([
        f_vect_polars[x_i['Q']] - Q_dot,
        f_vect_polars[x_i['P']] - P_dot,
        f_vect_polars[x_i['R']] - R_dot],
        [delta_e, delta_a, delta_r],
        simplify=False)

    for key in [delta_e, delta_a, delta_r]:
        print 'simplifying', key
        sys.stdout.flush()
        invert_sol_att[key] = \
            invert_sol_att[key].expand().factor().trigsimp()

    print 'inverting position dynamics'
    sys.stdout.flush()
    invert_sol_delta_t = sympy.solve(
        f_vect_polars[x_i['V_T']] - VT_dot, delta_t,
        simplify=False)[0].expand().factor().trigsimp()

    return {
        'delta_e': invert_sol_att[delta_e],
        'delta_a': invert_sol_att[delta_a],
        'delta_r': invert_sol_att[delta_r],
        'delta_t': invert_sol_delta_t,
    }


def create_inner_controller(
        dynamics, inversion, constants,
        P_pole=10, Q_pole=10, R_pole=10,
        VT_pole=1, VT_cmd=10):

    x = sympy.DeferredVector('x')
    x_i = dynamics['x_i']

    P = x[x_i['P']]
    Q = x[x_i['Q']]
    R = x[x_i['R']]
    VT = x[x_i['V_T']]

    x_sub = dynamics['x_sub']
    xd_sub = {
        'P_dot': -P_pole*P,
        'Q_dot': -Q_pole*Q,
        'R_dot': -R_pole*R,
        'VT_dot': -VT_pole*(VT - VT_cmd),
    }

    comb_subs = {}
    comb_subs.update(x_sub)
    comb_subs.update(xd_sub)
    comb_subs.update(constants)

    delta_e = inversion['delta_e'].subs(comb_subs)
    delta_t = inversion['delta_t'].subs(comb_subs)
    delta_a = inversion['delta_a'].subs(comb_subs)

    f = sympy.lambdify(
        (x),
        sympy.Matrix([delta_e, delta_t, delta_a]))
    # convert to numpy 1D array, defaults to numpy matrix
    return lambda x: np.array(f(x))[:, 0]


def create_outer_controller(
        dynamics, inversion, constants, VT_cmd,
        h_pole=1, psi_pole=1, VT_pole=1):

    x = sympy.DeferredVector('x')
    x_i = dynamics['x_i']
    x_sub = dynamics['x_sub']

    desired_dynamics_sub = {
        'h_dot': -h_pole*x[x_i['phi']],
        'psi_dot': -psi_pole*x[x_i['psi']],
        'VT_dot': -VT_pole*(x[x_i['V_T']] - VT_cmd),
    }

    delta_e = inversion['delta_e'].\
        subs(constants).sub(desired_dynamics_sub).subs(x_sub)
    delta_t = inversion['delta_t'].\
        subs(constants).sub(desired_dynamics_sub).subs(x_sub)
    delta_a = inversion['delta_a'].\
        subs(constants).sub(desired_dynamics_sub).subs(x_sub)

    f = sympy.lambdify(
        (x),
        sympy.Matrix([delta_e, delta_t, delta_a]))
    # convert to numpy 1D array, defaults to numpy matrix
    return lambda x: np.array(f(x))[:, 0]
