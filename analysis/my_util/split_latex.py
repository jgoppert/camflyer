import sympy
from IPython.display import Latex

def by_terms(lhs, expr,terms_per_line=1):
    """
    This function is written to handle displaying long latex equations.
    """
    latex = r'\begin{align*}' + '\n'
    terms = expr.as_ordered_terms()
    n_terms = len(terms)
    term_count = 1
    for i in range(n_terms):
        term = terms[i]
        term_start = r''
        term_end = r''
        sign = r'+'
        if term_count > terms_per_line:
            term_start = '&'
            term_count = 1
        if term_count == terms_per_line:
            term_end = r'\ldots \\' + '\n'
        if term.as_ordered_factors()[0]==-1:
            term = -1*term
            sign = r'-'
        if i == 0: # beginning
            latex += r'{:s} =& {:s} {:s} {:s}'.format(sympy.latex(lhs),
                                                     term_start,sympy.latex(term),term_end)
        elif i == n_terms-1: # end
            term_end = '\n'
            latex += r'{:s} {:s} {:s} {:s}'.format(term_start,sign,sympy.latex(term),term_end)
        else: # middle
            latex += r'{:s} {:s} {:s} {:s}'.format(term_start,sign,sympy.latex(term),term_end)
        term_count += 1
    latex += r'\end{align*}' + '\n'
    return Latex(latex)

def by_factors(lhs, expr,terms_per_line=1):
    """
    This function is written to handle displaying long latex equations.
    """
    latex = r'\begin{align*}' + '\n'
    terms = expr.as_ordered_factors()
    n_terms = len(terms)
    term_count = 1
    for i in range(n_terms):
        term = terms[i]
        term_start = r''
        term_end = r''
        if term_count > terms_per_line:
            term_start = '&'
            term_count = 1
        if term_count == terms_per_line:
            term_end = r'\ldots \\' + '\n'
        if term.as_ordered_factors()[0]==-1:
            term = -1*term
        if i == 0: # beginning
            latex += r'{:s} =& {:s} {:s} {:s}'.format(sympy.latex(lhs),
                                                     term_start,sympy.latex(term),term_end)
        elif i == n_terms-1: # end
            term_end = '\n'
            latex += r'{:s} {:s} {:s}'.format(term_start,sympy.latex(term),term_end)
        else: # middle
            latex += r'{:s} {:s} {:s}'.format(term_start,sympy.latex(term),term_end)
        term_count += 1
    latex += r'\end{align*}' + '\n'
    return Latex(latex)
