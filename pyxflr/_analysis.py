import csv
import numpy
import matplotlib.pyplot as plt
import re

def read_polar_csv(filename):
    """
    read polar csv file from xflr
    """
    with open(filename, 'rb') as csvfile:
        dialect = csv.excel
        dialect.skipinitialspace = True
        reader = csv.reader(csvfile, dialect=dialect)
        found_header = False
        data_rows = []
        for row in reader:
            if len(row) <= 0:
                continue
            if found_header == False:
                if row[0] == 'alpha':
                    found_header = True
                    headers = row
                    reader.next()
            else:
                data_row = numpy.zeros((1,len(row)))
                for j in range(len(row)):
                    data_row[0,j] = float(row[j])
                data_rows.append(data_row)
        if len(data_rows) != 0:
            data = numpy.concatenate(data_rows,axis=0)
    data_dict = {}
    for i in range(len(headers)):
        data_dict[headers[i].strip()] = data[:,i]
    return data_dict

def plot_polar(data):
    """
    plot polar csv file from xflr
    """
    figs = {}

    figs['CLAlpha'] = plt.figure("CL vs. Alpha")
    plt.title('Lift Coefficient vs. Angle of Attack')
    plt.plot(data['alpha'],data['CL'])
    plt.xlabel('$\\alpha$, deg')
    plt.ylabel('$C_L$')
    plt.grid()

    figs['CLCD'] = plt.figure("CLCD")
    plt.title('Drag Coefficient vs. Lift Coefficient')
    hICd = plt.plot(data['CL'], data['ICd'],'k--')
    hPCd = plt.plot(data['CL'],data['PCd'],'k-.')
    hTCd = plt.plot(data['CL'],data['TCd'],'')
    plt.legend((hICd[0], hPCd[0], hTCd[0]),
               ('induced', 'parasitic', 'total'), loc='best')
    plt.xlabel('$C_L$')
    plt.ylabel('$C_D$')
    plt.grid()

    figs['CmAlpha'] = plt.figure("Cm vs. Alpha")
    plt.title('Pitch Moment vs. Angle of Attack')
    hGCm = plt.plot(data['alpha'],data['GCm'])
    plt.xlabel('$\\alpha$, deg')
    plt.ylabel('$C_m$')
    plt.grid()

    figs['XCPAlpha'] = plt.figure("XCP vs. Alpha")
    plt.title('Center of Pressure')
    hGCm = plt.plot(data['alpha'], 100*data['XCP'])
    plt.xlabel('$\\alpha$, deg')
    plt.ylabel('$X_{CP}$, cm')
    plt.grid()

    return figs

def read_stability(filename):
    """
    Read stability info from log
    """
    data_dict = {}
    data_dict['modes'] = []
    with open(filename, 'rb') as stabfile:
        data_line_re = re.compile(\
            r' *(\S+(\s+\S+)*) *= *([+/-]? *\d+\.?\d*) *'\
            '(([+/-]) *(\d+\.?\d*)i)? *(.*)?')
        header_line_re = re.compile('(\S+(\s+\S+)*)')
        mode_count = 0
        data_dict = {'general':{},
            'stability derivs':{},
            'control derivs':{},
            'longitudinal modes':[],
            'lateral modes':[]}
        current = 'general'
        for line in stabfile.readlines():
            match_data = re.match(data_line_re, line)
            if match_data is not None:
                name = match_data.group(1)
                real_part = match_data.group(3)
                cmpl_sign = match_data.group(5)
                cmpl_part = match_data.group(6)
                unit = match_data.group(7)

                if cmpl_part is None:
                    val = float(real_part)
                else:
                    val = complex(float(real_part), float(cmpl_part))

                if name == 'Eigenvalue':
                    data_dict[current].append({})
                    mode_count += 1

                if mode_count > 0:
                    data_dict[current][mode_count-1][name] = \
                        {'val': val, 'unit':unit}
                else:
                    data_dict[current][name] = {'val':val,'unit':unit}
            else:
                match_header = re.match(header_line_re, line)
                if match_header is not None:
                    header = match_header.group(1)
                    if header == 'Non-dimensional Stability Derivatives:':
                        current = 'stability derivs'
                    elif header == 'Non-dimensional Control Derivatives:':
                        current = 'control derivs'
                    elif header == 'Longitudinal modes:':
                        current = 'longitudinal modes'
                        mode_count = 0
                    elif header == 'Lateral modes:':
                        current = 'lateral modes'
                        mode_count = 0
    return data_dict

if __name__ == "__main__":
    polar = read_polar_csv('T1-9.0 m s-VLM1.csv')
    plot_polar(polar)
    stab = read_stability('stability-analysis.txt')

# vim:ts=4:sw=4:expandtab
