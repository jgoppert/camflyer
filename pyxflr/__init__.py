"""
This module is built to read data from xlfr
It current works with xflr5.
"""
import matplotlib.pyplot as plt
from _analysis import read_polar_csv, plot_polar, read_stability
