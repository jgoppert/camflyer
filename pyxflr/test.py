# -*- coding: iso-8859-15 -*-
from _analysis import read_polar_csv, plot_polar, read_stability
import unittest
import os

this_dir, this_filename = os.path.split(__file__)


class Analysis(unittest.TestCase):

    def test_read_polar_csv(self):
        read_polar_csv(os.path.join(
            this_dir, "data", "T1-9.0 m s-VLM1.csv"))
        read_polar_csv(os.path.join(
            this_dir, "data", "T1-10.6 m s-VLM2.csv"))
        read_polar_csv(os.path.join(
            this_dir, "data", "T5-a4.3°-10.6m s-VLM2.csv"))

    def test_plot_polar(self):
        polar = read_polar_csv(os.path.join(
            this_dir, "data", "T1-9.0 m s-VLM1.csv"))

        plot_polar(polar)

    def test_read_stability(self):
        read_stability(os.path.join(
            this_dir, "data", "stability-analysis.txt"))
        read_stability(os.path.join(
            this_dir, "data", "elevator_analysis.txt"))
        read_stability(os.path.join(
            this_dir, "data", "aileron_analysis.txt"))

# vim:ts=4:sw=4:expandtab
