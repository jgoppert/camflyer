import matplotlib.pyplot as plt
import itertools
import matplotlib
import numpy as np
import os

from nonlinear_model import Aircraft, Data

figpath = os.path.join('data', 'figures')

# do simulation
ac = Aircraft()
ac.simulate()
data = ac.data

# extract data, setup parameter access
t = data.t
x = data.x.view(dtype=Data.x_dtype, type=np.recarray)
y = data.y.view(dtype=Data.y_dtype, type=np.recarray)
u = data.u.view(dtype=Data.u_dtype, type=np.recarray)
y_update = data.y_update.view(
    dtype=Data.y_update_dtype, type=np.recarray)

plt.close('all')
plt.rcParams['lines.linewidth'] = 5
plt.rcParams['lines.markersize'] = 10
plt.rcParams['font.size'] = 15

plt.figure()
h = plt.plot(t, np.rad2deg(np.column_stack((x.P, x.Q, x.R))))
plt.grid()
plt.title('rotation rate')
plt.xlabel('t (sec)')
plt.ylabel('(deg/s)')
plt.legend(['$P$', '$Q$', '$R$'], loc='best')
plt.savefig(os.path.join(figpath, 'rotation_rate.pdf'))

plt.figure()
h = plt.plot(t, np.column_stack((x.p_N, x.p_E, x.h)))
plt.legend(['$p_N$', '$p_E$', 'h'], loc='best')
plt.grid()
plt.xlabel('t (sec)')
plt.ylabel('(m)')
plt.title('position')
plt.savefig(os.path.join(figpath, 'position.pdf'))

plt.figure()
h = plt.plot(t, y.V_T)
plt.legend(h, ['$V_T$'], loc='best')
plt.grid()
plt.xlabel('t (sec)')
plt.ylabel('(m/s)')
plt.title('$V_T$')
plt.savefig(os.path.join(figpath, 'V_T.pdf'))

plt.figure()
h = plt.plot(t, np.rad2deg(np.column_stack((y.alpha, y.beta))))
plt.legend(h, ['$\\alpha$', '$\\beta$'], loc='best')
plt.xlabel('t (sec)')
plt.ylabel('(deg)')
plt.grid()
plt.title('$\\alpha$, $\\beta$')
plt.savefig(os.path.join(figpath, 'alpha_beta.pdf'))

plt.figure()
h = plt.plot(t, np.rad2deg(np.column_stack((x.x_l, x.x_r))))
hc = plt.plot(
    t,
    np.rad2deg(np.column_stack((u.delta_l, u.delta_r))), '--')
plt.legend(
    [h[0], h[1], hc[0], hc[1]],
    ['$\\delta_l$', '$\\delta_r$',
        '$ref_l$', '$ref_r$'], loc='best')
plt.xlabel('t (sec)')
plt.ylabel('(deg)')
plt.grid()
plt.title('elevons')
plt.savefig(os.path.join(figpath, 'elevons.pdf'))

plt.figure()
h = plt.plot(t, x.x_t)
hc = plt.plot(t, u.delta_t, '--')
plt.legend([h[0], hc[0]], ['$\\delta_t$', '$ref_t$'], loc='best')
plt.xlabel('t (sec)')
plt.ylabel('(deg)')
plt.grid()
plt.title('propulsion')
plt.savefig(os.path.join(figpath, 'propulsion.pdf'))

plt.figure()
h = plt.plot(t, np.rad2deg(np.column_stack((
    y.phi, y.theta, y.psi))))
plt.legend(h, ['$\\phi$', '$\\theta$', '$\\psi$'], loc='best')
plt.xlabel('t (sec)')
plt.ylabel('(deg)')
plt.grid()
plt.title('euler angles')
plt.savefig(os.path.join(figpath, 'euler_angles.pdf'))


def plot_updates(updates, n_t):
    fillstyle = itertools.cycle(matplotlib.lines.Line2D.fillStyles)
    h = []
    count = 0
    for update in updates:
        i_update = np.where(update[:n_t])[0]
        h.append(plt.plot(
            t[i_update],
            count*np.ones(len(i_update)), '.',
            markersize=20,
            marker='o',
            fillstyle=fillstyle.next())[0])
        plt.gca().get_yaxis().set_visible(False)
        count -= 1
    plt.xlim(0, t[n_t])
    plt.ylim(1, count)
    return h

plt.figure()
h = plot_updates(
    [y_update.gyro, y_update.mag, y_update.accel],
    min(len(t), 101))
plt.legend(h, ['gyro', 'mag', 'accel'], ncol=3, loc='best')
plt.xlabel('t (sec)')
plt.grid()
plt.title('fast updates')
plt.savefig(os.path.join(figpath, 'fast_updates.pdf'))

plt.figure()
h = plot_updates(
    [y_update.gps, y_update.pitot, y_update.baro],
    min(len(t), 1001))
plt.legend(h, ['gps', 'pitot', 'baro'], ncol=3, loc='best')
plt.xlabel('t (sec)')
plt.grid()
plt.savefig(os.path.join(figpath, 'slow_updates.pdf'))
